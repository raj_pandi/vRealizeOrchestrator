# Hardware Requirements
 - 2 CPU 
 
 - 6GB of memory
 
 - 17GB hard disk
 
# Operating System Support
- You can install vRealize Orchestrator only on 64-bit operating systems.

# Import workflow
Website link: https://docs.vmware.com/en/vRealize-Orchestrator/7.3/com.vmware.vrealize.orchestrator-using-client.doc/GUID-F001DE13-07F9-4904-B028-5F8BDEDC90E4.html



# Description
list_vm_snap.workflow: This workflow got validated successfully and when you run, It will print out the names of VM's that contains snapshot, and datastore size in GB's.
